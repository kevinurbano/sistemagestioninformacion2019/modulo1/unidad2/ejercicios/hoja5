﻿USE ciclistas;

/* Modulo 1 Unidad 2 */

/* Consulta de combinacion externa 5 */

/* 
  Consulta 1 - Nombre y edad de los ciclistas que NO han ganado etapas.
*/
  -- C1
    SELECT 
      DISTINCT e.dorsal 
      FROM 
        etapa e;

  -- Final
    SELECT 
      c.nombre,c.edad
      FROM 
        ciclista c
      LEFT JOIN
        (
          SELECT 
            DISTINCT e.dorsal 
            FROM 
              etapa e
        ) c1 
      ON c.dorsal=c1.dorsal
      WHERE c1.dorsal IS NULL;

/*
  Consulta 2 - Nombre y edad de los ciclistas que NO han ganado puertos.   
*/
  -- C1
  SELECT 
    DISTINCT p.dorsal
    FROM 
      puerto p;

  -- Final
  SELECT 
    c.nombre,c.edad
    FROM ciclista c
    LEFT JOIN 
      (
        SELECT 
          DISTINCT p.dorsal
          FROM 
            puerto p
      ) c1 
    ON c.dorsal=c1.dorsal
    WHERE c1.dorsal IS NULL;


/*
  Consulta 3 - Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA etapa.   
*/
  -- C1
    SELECT 
      DISTINCT e.dorsal 
      FROM 
        etapa e; 
  
  -- C2
    SELECT 
      DISTINCT c.nomequipo
      FROM 
        ciclista c
      LEFT JOIN 
        (
          SELECT 
            DISTINCT e.dorsal 
            FROM 
              etapa e
        ) c1
      ON c.dorsal=c1.dorsal
      WHERE c1.dorsal IS NULL;

  -- Final
    SELECT 
      distinct e.director 
      FROM 
        equipo e
      JOIN 
        (
          SELECT 
            DISTINCT c.nomequipo
            FROM 
              ciclista c
            LEFT JOIN 
              (
                SELECT 
                  DISTINCT e.dorsal 
                  FROM 
                    etapa e
              ) c1
            ON c.dorsal=c1.dorsal
            WHERE c1.dorsal IS NULL
        ) c1 ON e.nomequipo=c1.nomequipo;

/*
  Consulta 4 - Dorsal y nombre de los ciclistas que NO hayan llevado algún maillot.
*/
  -- C1
    SELECT 
      DISTINCT l.dorsal
      FROM 
        lleva l;

  -- Final
    SELECT 
      c.dorsal,c.nombre
      FROM 
        ciclista c
      LEFT JOIN 
        (
          SELECT 
            DISTINCT l.dorsal
            FROM 
              lleva l
        ) c1
      ON c.dorsal=c1.dorsal
      WHERE c1.dorsal IS NULL;

/*
  Consulta 5 - Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA.
*/
  -- C1
    SELECT 
      DISTINCT l.dorsal 
      FROM
        lleva l
      JOIN
        maillot m
      ON l.código = m.código
      WHERE m.color='amarillo';

  -- Final
    SELECT 
      c.dorsal,c.nombre 
      FROM 
        ciclista c
      LEFT JOIN
        (
          SELECT 
            DISTINCT l.dorsal 
            FROM
              lleva l
            JOIN
              maillot m
            ON l.código = m.código
            WHERE m.color='amarillo'
        ) c1
      ON c.dorsal=c1.dorsal
      WHERE c1.dorsal IS NULL;

/*
  Consulta 6 - Indicar el numetapa de las etapas que NO tengan puertos.
*/
  -- C1
    SELECT 
      DISTINCT p.numetapa 
      FROM 
        puerto p;

  -- Final
    SELECT 
      e.numetapa
      FROM 
        etapa e
      LEFT JOIN
        (
          SELECT 
            DISTINCT p.numetapa 
            FROM 
              puerto p
        ) c1
      ON e.numetapa=c1.numetapa
      WHERE
        c1.numetapa IS NULL;

/*
  Consulta 7 - Indicar la distancia media de las etapas que NO tengan puertos.
*/
  -- Utilizo la consulta anterior 
  -- C1
    SELECT 
      DISTINCT p.numetapa 
      FROM 
        puerto p;

  -- Final
    SELECT 
      AVG(e.kms) distanciaMedia
      FROM 
        etapa e
      LEFT JOIN
        (
          SELECT 
            DISTINCT p.numetapa 
            FROM 
              puerto p
        ) c1
      ON e.numetapa=c1.numetapa
      WHERE 
        c1.numetapa IS null;
 
/*
  Consulta 8 - Listar el número de ciclistas que NO hayan ganado alguna etapa.  
*/
  -- C1
    SELECT 
      DISTINCT e.dorsal 
      FROM 
        etapa e;

  -- Final
    SELECT 
      COUNT(*) nCiclistas
      FROM 
        ciclista c
      LEFT JOIN
        (
          SELECT 
            DISTINCT e.dorsal 
            FROM 
              etapa e
        ) c1 
      ON c.dorsal=c1.dorsal
      WHERE c1.dorsal IS NULL;

/*
  Consulta 9 - Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto 
*/
  -- C1 - Ciclistas que han ganado etapa que no tenga puerto
    SELECT 
      DISTINCT dorsal
      FROM 
        etapa e
      LEFT JOIN
        (
          SELECT 
            DISTINCT p.numetapa 
            FROM 
              puerto p
        ) c1
      ON e.numetapa=c1.numetapa
      WHERE 
        c1.numetapa IS null;

/*
  Consulta 10 - Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos 
*/

  -- C1
  SELECT 
    DISTINCT e.dorsal 
    FROM 
      etapa e;


  -- C2
    SELECT 
      e.dorsal 
      FROM 
        puerto p 
      JOIN 
        etapa e 
      ON p.numetapa = e.numetapa;

  -- Final
  SELECT 
    c1.dorsal
    FROM 
      (
        SELECT 
          DISTINCT e.dorsal 
          FROM 
            etapa e
      ) c1
      LEFT JOIN
      (
        SELECT 
          e.dorsal 
          FROM 
            puerto p 
          JOIN 
            etapa e 
          ON p.numetapa = e.numetapa
      ) c2
    ON c1.dorsal=c2.dorsal
    WHERE c2.dorsal IS NULL